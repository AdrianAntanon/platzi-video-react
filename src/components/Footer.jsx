import React from 'react'
import '../assets/styles/components/Footer.scss';

const Footer = () => {
    const terms = 'Términos de uso';
    const privacy = 'Declaración de privacidad';
    const help = 'Centro de ayuda';
    return (
        <footer className="footer">
            <a href="/">{terms} </a>
            <a href="/">{privacy} </a>
            <a href="/">{help} </a>
        </footer>
    );
}


export default Footer;
export const SET_FAVORITE = 'SET_FAVORITE';
export const DELETE_FAVORITE = 'DELETE_FAVORITE';
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGOUT_REQUEST = 'LOGOUT_REQUEST';
export const REGISTER_REQUEST = 'REGISTER_REQUEST';
export const GET_VIDEO_SOURCE = 'GET_VIDEO_SOURCE';

export const setFavorite = payload => {
  return { type: SET_FAVORITE, payload }
};

export const deleteFavorite = payload => {
  return { type: DELETE_FAVORITE, payload }
};

export const loginRequest = payload => ({
  type: LOGIN_REQUEST, payload
});

export const logoutRequest = payload => {
  return { type: LOGOUT_REQUEST, payload }
};

export const registerRequest = payload => {
  return { type: REGISTER_REQUEST, payload }
};

export const getVideoSource = payload => {
  return { type: GET_VIDEO_SOURCE, payload }
};